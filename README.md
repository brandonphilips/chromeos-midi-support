## ChromeOS MIDI Support Information

This document tracks MIDI device and application support for ChromeOS.

Have an application you want tested? Let me know! Did you test an app? File a pull request!

### Android Applications

Applications using the `android.software.midi` interface seem to work consistently. You can test your hardware using the [test procedures](https://source.android.com/devices/audio/midi_test) on the Android docs. This is the [source to the test application](https://github.com/googlesamples/android-MidiScope) and the [Play Store](https://play.google.com/store/apps/details?id=com.mobileer.example.midiscope) link.

Unfortunately, many apps in the Play Store use the `android.hardware.usb.host feature` to build MIDI support. This older API is not supported by ChromeOS as documented in [the Android on ChromeOS docs](https://developer.android.com/topic/arc/manifest). Here is an [example driver](https://github.com/kshoji/USB-MIDI-Driver/blob/ac44b689d2f7218e48a0b6814183c9ea70a6264e/MIDIDriverSample/AndroidManifest.xml#L7) for Android using this API.

| App Name | Hardware | Works | Explanation |
| -------- | --------:| -----:| -----------:|
| [Perfect Piano](https://play.google.com/store/apps/details?id=com.gamestar.perfectpiano) | Yamaha CP50 | Yes | | 
| [Simply Piano](https://play.google.com/store/apps/details?id=com.joytunes.simplypiano) | Yamaha CP50 | No | uses android.hardware.usb.host |
| [Caustic 3](https://play.google.com/store/apps/details?id=com.singlecellsoftware.caustic) | Yamaha CP50 | No | uses android.hardware.usb.host |

### Web Applications

As expected [Web Audio](https://webaudio.github.io/web-midi-api/) APIs work well with ChromeOS. But, for completeness here are some web apps I have tried. If you have success or failures please submit your own!

| App Name | Hardware | Works | Explanation |
| -------- | --------:| -----:| -----------:|
| [cwilso/minisynth](https://webaudiodemos.appspot.com/midi-synth/index.html) | Yamaha CP50 | Yes | | 
| [Soundtrap](https://www.soundtrap.com) | Yamaha CP50 | Yes | |
